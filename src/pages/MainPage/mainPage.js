import React from 'react'

import Navbar from './../../components/Navbar/navbar'

import { PageDiv, Img } from './styles'

function MainPage () {
    const [flag, setFlag] = React.useState(false)

    React.useEffect(() => {
        const interval = setInterval(() => {
            setFlag(flag => !flag)
        }, 1000);
        return () => clearInterval(interval)
    }, [])

    return (
        <>
            <Navbar />
            <PageDiv color={flag}>
                BEM VINDO AO MEU SITE ONDE EU FALO SOBRE MUITAS COISA DIFERENTES E TECNOLOGICAS E<br/>CLUBPENGUIN
                <Img src="https://toppng.com/uploads/preview/file-history-steven-universe-1156290095261iaa8uyqk.png" x='300px' y='1000px'/>
            </PageDiv>
        </>
    )
}

export default MainPage