import styled, { keyframes } from 'styled-components'

export const PageDiv = styled.div`
    background-color: ${props => props.color ? 'blue' : 'yellow'};
    color: ${props => props.color ? 'white' : 'black'};
    height: 2000px;
    bottom: 0;

    font-size: 100px;
    transition-duration: 500ms;
`
const spin = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

export const Img = styled.img`
    animation-name: ${spin};
    animation-duration: 5000ms;
    animation-iteration-count: infinite;
    animation-timing-function: linear;

    position: absolute;
    top:${props => props.x};
    left:${props => props.y};
    height: 600px;
    width: 600px;
`