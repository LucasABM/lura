import React from 'react';
import { Router } from '@reach/router'

import MainPage from './pages/MainPage/mainPage'

import './App.css';

function App() {
  return (
    <Router>
      <MainPage path="/" default/>
      {/* adicionar uma página 404 como default */}
    </Router>
  )
}

export default App;
