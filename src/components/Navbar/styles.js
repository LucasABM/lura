import styled from 'styled-components'

export const NavbarDiv = styled.div`
    position: sticky;
    top: 0;
    left: 0;
    right: 0;
    
    height: 100px;
    background-color: red;
    
    display: flex;
    justify-content: space-between;
`

export const TitleDiv = styled.div`
    font-family: algerian;
    font-size: 50px;
    color: white;

    padding-left: 100px;
    margin: auto 0;
`

export const MenuDiv = styled.div`
    color: lightgreen;
    font-size:45px;

    padding-right: 100px;

    display: flex;
`

export const OptionDiv = styled.div`
    margin: auto 20px;
`