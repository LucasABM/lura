import React from 'react'
import { NavbarDiv, TitleDiv, MenuDiv, OptionDiv } from './styles'

function Navbar () {

    return (
        <NavbarDiv>
            <TitleDiv>
                LuRa
            </TitleDiv>
            <MenuDiv>
                <OptionDiv>
                    Página Inicial
                </OptionDiv>
                <OptionDiv>
                    Sobre Nós
                </OptionDiv>
                <OptionDiv>
                    Login
                </OptionDiv>
            </MenuDiv>
        </NavbarDiv>
        
        
    )
}

export default Navbar